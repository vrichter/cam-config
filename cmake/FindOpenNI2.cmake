#*********************************************************************
#**                                                                 **
#** File   : cmake/FindOpenNI2.cmake                                **
#** Authors: Viktor Richter                                         **
#**                                                                 **
#**                                                                 **
#** GNU LESSER GENERAL PUBLIC LICENSE                               **
#** This file may be used under the terms of the GNU Lesser General **
#** Public License version 3.0 as published by the                  **
#**                                                                 **
#** Free Software Foundation and appearing in the file LICENSE.LGPL **
#** included in the packaging of this file.  Please review the      **
#** following information to ensure the license requirements will   **
#** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
#**                                                                 **
#*********************************************************************

#  This will set
#
#  OpenNI2_FOUND - system has OpenNI2
#  OpenNI2_INCLUDE_DIRS - the OpenNI2 include directories
#  OpenNI2_LIBRARIES - link these to use OpenNI2
#  OpenNI2_LINK_DIRS - the directories to link against
#  OpenNI2_DEFINES - the OpenNI2 definitions to pass

include(cmake/FindPkgConfig.cmake)

include(cmake/LibFindMacros.cmake)

# Include dir
find_path(OpenNI2_INCLUDE_DIR
  NAMES OpenNI.h
  PATHS "${OpenNI2_DIR}/Include/" "${OpenNI2_DIR}/include/"
)

# Find the libraries
find_library(OpenNI2_LIBRARY
  NAMES libOpenNI2.so
  PATHS "${OpenNI2_DIR}/Redist/" "${OpenNI2_DIR}/lib/"
)

# Set the include dir variables and the libraries.
set(OpenNI2_PROCESS_INCLUDES OpenNI2_INCLUDE_DIR)
set(OpenNI2_PROCESS_LIBS OpenNI2_LIBRARY)
libfind_process(OpenNI2)
if(OpenNI2_FOUND)
 set(OpenNI2_DEFINES "${OpenNI2_DEFINES} -DHAVE_OPENNI2")
endif()

