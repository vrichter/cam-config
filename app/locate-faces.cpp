/********************************************************************
**                                                                 **
** File   : app/locate-faces                                       **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <mutex>

#include <boost/program_options.hpp>

#include <rsb/Factory.h>
#include <rsb/Listener.h>

#include <rst/vision/Faces.pb.h>
#include <rst/vision/LocatedFaces.pb.h>

#include <transform/FacesHandler.h>

#include <utils/RsbHelpers.h>
#include <utils/Types.h>
#include <utils/Macros.h>
INITIALIZE_EASYLOGGINGPP


int main(int argc, char **argv){
  boost::program_options::variables_map program_options;

  std::string description = "This application listens for published faces on a preconfigured "
    "camera and creates located face hypotheses with a position estimation.";
  std::stringstream description_text;
  description_text << description << "\n\n" << "Allowed options";
  boost::program_options::options_description desc(description_text.str());
  desc.add_options()
      ("help,h","produce help message")

      ("camera-config,c",
       boost::program_options::value<std::string>()->default_value(""),
       "The camera configuration is needed to estimate a world face position from a rectangle in image coordinates.")

      ("input-scope,i",
       boost::program_options::value<std::string>()->default_value("/home/kitchen/faces"),
       "The input scope to get faces percepts.")

      ("output-scope,o",
       boost::program_options::value<std::string>()->default_value("/home/kitchen/locatedfaces"),
       "The output scope to publish located face percepts.")

      ("face-width-mm,w",
       boost::program_options::value<uint>()->default_value(transform::FacesHandler::default_face_width_mm()),
       "The width of a human face in mm. Can be used to adapt to properties of different face detectors.")

      ;

  try {
    boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), program_options);
    boost::program_options::notify(program_options);

    std::stringstream arguments;
    for(int i = 0; i < argc; ++i){
      arguments << argv[i] << " ";
    }
    LOG_INFO << "Program started with line: " << arguments.str();

    if (program_options.count("help")) {
      std::cout << desc << "\n";
      return 1;
    }

  } catch (boost::program_options::error& e) {
    std::stringstream arguments;
    for(int i = 0; i < argc; ++i){
      arguments << argv[i] << " ";
    }
    LOG_ERROR << "Could not parse program options: Line: " << arguments.str() << " Error: " << e.what();
    std::cout << "Could not parse program options: " << e.what();
    std::cout << "\n\n" << desc << "\n";
    return 1;
  }

  // init camera and calculate transformations and projections
  icl::geom::Camera camera(program_options["camera-config"].as<std::string>());

  utils::rsbhelpers::register_rst<
      rst::vision::Faces,
      rst::vision::LocatedFaces
      >();

  rsb::ListenerPtr listener = rsb::getFactory().createListener(program_options["input-scope"].as<std::string>());

  transform::PositionEstimation estimation(camera);

  auto handler = boost::shared_ptr<transform::FacesHandler>(new transform::FacesHandler(estimation,rsb::Scope(program_options["output-scope"].as<std::string>())));

  listener->addHandler(handler);

  // deadlock
  std::mutex lock;
  lock.lock();
  lock.lock();
}
