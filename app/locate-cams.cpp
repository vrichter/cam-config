/********************************************************************
**                                                                 **
** File   : app/locate-cams                                        **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <boost/program_options.hpp>
#include <utils/Types.h>
#include <utils/Macros.h>
#include <utils/RsbHelpers.h>
#include <transform/PositionProvider.h>
#include <extract/CorrespondenceDetector.h>

#include <boost/algorithm/string.hpp>

#include <mutex>

INITIALIZE_EASYLOGGINGPP

int main(int argc, char **argv){
  boost::program_options::variables_map program_options;

  std::string description = "This application collects points a set of coordinate systems to calculate "
                            "their relative transformations. For this to work one of the systems must "
                            "be the reference (global) system. All calculated transformations will be "
                            "relative to that.\n\n"
                            "This application makes the assumption that a point is produced by the same "
                            "feature when it is observed in both the global and local coordinate system. "
                            "Point correspondences are created when only one point per provider is generated.";
  std::stringstream description_text;
  description_text << description << "\n\n" << "Allowed options";
  boost::program_options::options_description desc(description_text.str());
  desc.add_options()
      ("help,h","produce help message")

      ("reference,r",
       boost::program_options::value<std::string>()->default_value("/home/person"),
       "The position providing reference scope.")

      ("scopes,s",
       boost::program_options::value<std::string>()->default_value("/home/kitchen/locatedfaces;"
                                                                   "/home/wardrobe/locatedfaces"),
       "Position providing scopes. Semicolon-separated.")
      ;

  try {
    boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), program_options);
    boost::program_options::notify(program_options);

    std::stringstream arguments;
    for(int i = 0; i < argc; ++i){
      arguments << argv[i] << " ";
    }
    LOG_INFO << "Program started with line: " << arguments.str();

    if (program_options.count("help")) {
      std::cout << desc << "\n";
      return 1;
    }

  } catch (boost::program_options::error& e) {
    std::stringstream arguments;
    for(int i = 0; i < argc; ++i){
      arguments << argv[i] << " ";
    }
    LOG_ERROR << "Could not parse program options: Line: " << arguments.str() << " Error: " << e.what();
    std::cout << "Could not parse program options: " << e.what();
    std::cout << "\n\n" << desc << "\n";
    return 1;
  }

  std::string reference = program_options["reference"].as<std::string>();
  std::vector<std::string> scopes;
  boost::split(scopes,program_options["scopes"].as<std::string>(),boost::is_any_of(";"));

  if(scopes.empty()){
    LOG_ERROR << "Could not parse rsb scopes from parameter: '" << program_options["scopes"].as<std::string>();
    std::cout << "Could not parse rsb scopes from parameter: '" << program_options["scopes"].as<std::string>()
              << std::endl;
    exit(-1);
  }

  std::vector<transform::PositionProvider::Ptr> providers;
  providers.push_back(std::make_shared<transform::RsbPositionProvider>(reference));
  for(auto scope : scopes){
    transform::PositionProvider::Ptr provider = std::make_shared<transform::RsbPositionProvider>(scope);
    providers.push_back(provider);
  }

  std::vector<extract::FeatureFilter::Ptr> filters;
  filters.reserve(providers.size());
  for(transform::PositionProvider::Ptr provider : providers){
    extract::FeatureFilter::Ptr filter = std::make_shared<extract::OneOrNothingFeatureFilter>();
    provider->connect([filter] (transform::TimedPoseSet poses) { filter->push(poses);});
    filters.push_back(filter);
  }

  typedef extract::SimpleCorrespondenceDetector<> SimpleDetector;
  std::vector<SimpleDetector::Ptr> detectors;
  detectors.reserve(providers.size()-1);
  for(auto it = filters.begin(); it != filters.end(); ++it){
    if(it == filters.begin()) continue;
    SimpleDetector::Ptr detector = std::make_shared<SimpleDetector>();
    filters.front()->connect([detector](transform::TimedPose pose){ detector->pushGlobal(pose);});
    (*it)->connect([detector](transform::TimedPose pose){ detector->pushLocal(pose);});
    detectors.push_back(detector);
  }

  for(auto detector : detectors){
    detector->connect([&detector](extract::PosePair pair) {
      std::cout << "From: " << detector
                << " f: " <<  pair.first
                << " s: " << pair.second << std::endl;
    });
  }



  // deadlock
  std::mutex lock;
  lock.lock();
  lock.lock();
}
