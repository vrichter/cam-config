/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/utils/ObjectRegistry.h                             **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <mutex>
#include <set>
#include <map>
#include <sstream>
#include <boost/optional.hpp>

namespace utils {

  /**
   * @brief The ObjectRegistry template is used to save and retrieve objects using an id.
   *
   * @tparam ObjectIdType the type by which objects are referenced.
   * @tparam ObjectType the referenced objects type. Must be copy contructable.
   */
  template<
      typename ObjectIdType,
      typename ObjectType>
  class ObjectRegistry {

  public:

    class Exception : public std::exception {
    public:
      Exception(const std::string& message) : m_Message(message) {}
      virtual const char* what() const throw() override { return m_Message.c_str(); }
    private:
      const std::string m_Message;
    };


    /**
     * @brief registerObject adds an object to the object registry.
     * @param id a unique id of this object.
     * @param object a object that is worth being known.
     * @return false if object could not be registered because id is already used.
     */
    bool registerObject(const ObjectIdType& id, ObjectType object){
      std::unique_lock<std::mutex> lock(m_Lock);
      if(m_Objects.find(id) == m_Objects.end()){
        m_Objects[id] = object;
        return true;
      } else {
        return false;
      }
    }

    /**
     * @brief removeObject removes an object from the registry.
     * @param id the id of the object to be removed
     * @return true if an object with the passed id was presend and could be removed. else false-
     */
    bool removeObject(const ObjectIdType& id){
      std::unique_lock<std::mutex> lock(m_Lock);
      auto it = m_Objects.find(id);
      if(it != m_Objects.end()){
        m_Objects.erase(it);
        return true;
      } else {
        return false;
      }
    }

    /**
     * @brief hasObject can be used to check if a object for an id is already registered.
     * @param id the id of the object.
     * @return true when a object with the passed id is registered.
     */
    bool hasObject(const ObjectIdType& id){
      std::unique_lock<std::mutex> lock(m_Lock);
      return m_Objects.find(id) != m_Objects.end();
    }

    /**
     * @brief getObject access registered objects.
     * @param id the id of the required object.
     * @return a object pointer. nullptr when not available.
     */
    boost::optional<ObjectType> getObject(const ObjectIdType& id){
      std::unique_lock<std::mutex> lock(m_Lock);
      auto it = m_Objects.find(id);
      if(it != m_Objects.end()){
        return boost::optional<ObjectType>(it->second);
      } else {
        return boost::optional<ObjectType>();
      }
    }

    /**
     * @brief registeredObjectIds access registered object ids.
     * @return a vector of registered object ids
     */
    std::set<ObjectIdType> registeredObjectIds(){
      std::unique_lock<std::mutex> lock(m_Lock);
      std::set<ObjectIdType> ids;
      for(auto it : m_Objects){
        ids.insert(it.first);
      }
    }

  private:
    /**
     * @brief m_Objects inner object map.
     */
    std::map<ObjectIdType,ObjectType> m_Objects;

    /**
     * @brief m_Lock used for concurrency.
     */
    std::mutex m_Lock;
  };


  /**
   * @brief StaticRegistry provides a statically (singleton) acessable Registry and a Registrar.
   *
   * The Registrar can be used to statically register objects by creating a static instance.
   *
   * @tparam IdType the type identifying objects.
   * @tparam ObjectType the type of the objects.
   * @tparam ScopeType can be used to specify a type in which scope this Registry should exist.
   *         This way multiple distiunct but static Registries of the same type can exist because
   *         the static registries have different scope types.
   */
  template<
    typename IdType,
    typename ObjectType,
    typename ScopeType = void
    >
  class StaticRegistry {
  public:

    class Exception : public ObjectRegistry<IdType,ObjectType>::Exception {
    public:
      Exception(const std::string& message) : ObjectRegistry<IdType,ObjectType>::Exception(message) {}

    private:
    };


    /**
     * @brief registry a static accessor to this inner registry.
     * @return a static instance of Registry.
     */
    static ObjectRegistry<IdType,ObjectType>& registry(){
      static ObjectRegistry<IdType,ObjectType> registry;
      return registry;
    }

    /**
     * @brief Registers objects in a static object registry.
     *
     * The Registrar uses a static registry to access objects of the passed ScopeType
     * to register the object passed to its constructor. An object can be registered by creating
     * a static instance of this.
     *
     */
    class Registrar {
    public:

      /**
       * @brief Registrar registers an object in the static registry.
       * @param id the object identifier
       * @param object the object to register.
       * @throw std::exception when the same id is registered multiple times.
       */
      Registrar(IdType id, ObjectType object){

        bool registered = registry().registerObject(id,object);
        if(!registered){
          std::stringstream message;
          message << "Object with id = '" << id << "' already registered.";
          throw Exception(message.str());
        }
      }
    };
  };

} // namespace utils
