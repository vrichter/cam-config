/********************************************************************
**                                                                 **
** File   : src/utils/Types.cpp                                    **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <utils/Types.h>

namespace utils {

void projectPointsToPlaneCoordinates(const std::vector<Vec3D> &points,
                                     const Vec3D& center,
                                     const Vec3D& normal,
                                     std::vector<Vec2D>& dest)
{
  dest.resize(points.size(),Vec2D(0,0));
  if(points.empty()) return;
  Vec3D xpos = (projectToNormalPlane(points[0],center,normal) - center);
  Vec3D xcoord = xpos.normalized();
  Vec3D ycoord = cross(xcoord,normal).normalized();
  dest[0][0] = xpos.length();
  dest[0][1] = 0;
  for(unsigned int i = 1; i < points.size(); ++i){
    xpos = (projectToNormalPlane(points[i],center,normal) - center);
    dest[i][0] = scalar(xpos,xcoord);
    dest[i][1] = scalar(xpos,ycoord);
  }
}

Mat3D toRotation(const Vec3D axis, float32 angle){
  float32 cosA = std::cos(angle);
  float32 cosB = 1-cosA;
  float32 sinA = std::sin(angle);
  float32 ux = axis[0];
  float32 uy = axis[1];
  float32 uz = axis[2];
  return Mat3D(cosA+ux*ux*cosB, ux*uy*cosB-uz*sinA, ux*uz*cosB+uy*sinA,
               uy*ux*cosB+uz*sinA, cosA+uy*uy*cosB, uy*uz*cosB-ux*sinA,
               uz*ux*cosB-uy*sinA, uz*uy*cosB+ux*sinA, cosA+uz*uz*cosB);
}

Mat3D toRotation(const Vec3D axis_times_angle){
  return toRotation(axis_times_angle.normalized(),
                    axis_times_angle.length());
}


void toAxisAngle(Mat3D rotation, Vec3D& axis, float32& angle){
  angle = std::acos((rotation.trace() - 1) / 2);
  if(isnan(angle)){
    angle = 0.; // acos(1) angle gets nan when trace of rotation is > 3
  }
  if(angle == 0){
    axis = Vec3D(1,0,0);
  } else {
    axis = Vec3D(rotation[7] - rotation[5],
                 rotation[2] - rotation[6],
                 rotation[3] - rotation[1]) * (1/(2*std::sin(angle)));
  }
}

// create 3D rotation to rotate b to a
Mat3D rotateVector(Vec3D a, Vec3D b){
  Vec3D axis = cross(a,b).normalized();
  Vec3D a2 = a.normalized();
  Vec3D b2 = b.normalized();
  float32 angle = std::acos((a2.transp()*b2)[0]);
  return toRotation(axis,angle).transp();
}

// create 3D rotation to rotate b to a. ensure b2 is transformed to a2
Mat3D rotateVector2(Vec3D a1, Vec3D a2, Vec3D b1, Vec3D b2){
  // ensure orthogonal vektors so rotations are independent
  Vec3D oa = cross(a1,a2).normalized();
  Vec3D ob = cross(b1,b2).normalized();
  Vec3D oaxis = cross(oa,ob);
  if(!oaxis.length()){
    // no rotation between vectors
    return Mat3D(1,0,0,0,1,0,0,0,1);
  } else {
    oaxis.normalize();
  }
  float32 oangle = std::acos((oa.transp()*ob)[0]);
  return rotateVector(a1,b1) * toRotation(oaxis,oangle).transp();
}

// create transformation that translates center to 0, rotates, translates back.
Mat4D createCenterRotation(const Mat3D& rot, const Vec3D& center){
  Vec3D rtt = (rot*(-center))+center;
  return Mat4D(rot[0], rot[1], rot[2], rtt[0],
               rot[3], rot[4], rot[5], rtt[1],
               rot[6], rot[7], rot[8], rtt[2],
               0,0,0,1);
}


const icl::geom::GeomColor getDefaultSegmentColor(int id)
{
  int i = id % 100;
  static std::map<int,icl::geom::GeomColor> colorMap;
  static icl::utils::Mutex colorMapLock;
  icl::utils::Mutex::Locker l(colorMapLock);
  if(colorMap.find(i) == colorMap.end()){
    if(i < 0){
      colorMap[i] = icl::geom::GeomColor(1.,1.,1.,1.);
    } else {
      int H = (int) (i * 35.) % 360;
      float S = 1.0 - i * 0.01;
      float hi = floor((float) H / 60.);
      float f = ((float) H / 60.) - hi;
      float pp = 1.0 - S;
      float qq = 1.0 - S * f;
      float tt = 1.0 - S * (1. - f);
      float r = 1.0;
      float g = 1.0;
      float b = 1.0;
      switch ((int) hi) {
        case 1:
          r = qq;
          b = pp;
          break;
        case 2:
          r = pp;
          b = tt;
          break;
        case 3:
          r = pp;
          g = qq;
          break;
        case 4:
          r = tt;
          g = pp;
          break;
        case 5:
          g = pp;
          b = qq;
          break;
        default: //case 0/6:
          g = tt;
          b = pp;
      }
      colorMap[i] = icl::geom::GeomColor(r,g,b,1.);
    }
  }
  return colorMap[i];
}

}
