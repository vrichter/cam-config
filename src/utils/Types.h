/********************************************************************
**                                                                 **
** File   : src/utils/Types.h                                      **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

// this header is used to define some types

#include <ICLCore/Types.h>
#include <ICLMath/FixedVector.h>
#include <ICLGeom/Geom.h>

namespace utils {

typedef icl::icl32f float32;

typedef icl::math::FixedColVector<double,2> Vec2D;
typedef icl::math::FixedColVector<double,3> Vec3D;
typedef icl::math::FixedColVector<double,4> Vec4D;

typedef icl::math::FixedMatrix<double,3,3> Mat3D;
typedef icl::math::FixedMatrix<double,4,4> Mat4D;

typedef icl::math::FixedColVector<double,4> Quaternion;

inline Vec3D toVec3D(const icl::math::FixedColVector<float,4>& vec){
  return Vec3D(vec[0], vec[1], vec[2]);
}

inline Vec3D toVec3D(const Vec4D& vec){
  return Vec3D(vec[0], vec[1], vec[2]);
}

inline Vec4D toPoint4D(const Vec3D& vec){
  return Vec4D(vec[0], vec[1], vec[2], 1);
}

inline Vec4D toVec4D(const Vec3D& vec){
  return Vec4D(vec[0], vec[1], vec[2], 0);
}

inline Mat3D toSkewSymmetricMat(const Vec3D& vec)
{
  return Mat3D(0, -vec[3], vec[2],
               vec[3], 0, -vec[1],
               -vec[2], vec[1], 0);
}

/// homogeneous 3D cross-product
inline Vec3D cross(const Vec3D &v1, const Vec3D &v2){
  return Vec3D(v1[1]*v2[2]-v1[2]*v2[1],
               v1[2]*v2[0]-v1[0]*v2[2],
               v1[0]*v2[1]-v1[1]*v2[0]);
}

inline float32 scalar(const Vec2D a, const Vec2D b){
  return a[0]*b[0]+a[1]*b[1];
}

inline float32 scalar(const Vec3D a, const Vec3D b){
  return a[0]*b[0]+a[1]*b[1]+a[2]*b[2];
}

inline float32 scalar(const Vec4D a, const Vec4D b){
  return a[0]*b[0]+a[1]*b[1]+a[2]*b[2]+a[3]*b[3];
}

/// returns the projection of point  to the plane described by planepoint & normal
inline Vec3D projectToNormalPlane(Vec3D point, Vec3D planepoint, Vec3D normal){
  /* point = T, planepoint = P, normal = N
    S = T - (d*N) Punkt auf Ebene ist Punkt - Abstand zur Ebene * Normale
    PT = T - P vektor von ebenenpunkt auf Punkt PT
    d = PT*N PT projeziert auf normale (T's abstand zur ebene)
    S = T - (((T - P)*N)*N)
  */
  return point - (normal*scalar(normal,(point - planepoint)));
}

void projectPointsToPlaneCoordinates(const std::vector<Vec3D> &points,
                                     const Vec3D& center,
                                     const Vec3D& normal,
                                     std::vector<Vec2D>& dest);

Mat3D toRotation(const Vec3D axis, float32 angle);

void toAxisAngle(Mat3D rotation, Vec3D& axis, float32& angle);

// create 3D rotation to rotate b to a
Mat3D rotateVector(Vec3D a, Vec3D b);

// create 3D rotation to rotate b to a. ensure b2 is transformed to a2
Mat3D rotateVector2(Vec3D a1, Vec3D a2, Vec3D b1, Vec3D b2);

// create transformation that translates center to 0, rotates, translates back.
Mat4D createCenterRotation(const Mat3D& rot, const Vec3D& center);

// returns the angle between the two vectors in radians
inline double angle(Vec3D a, Vec3D b){
  //return acos(((a.transp()*b)/(a.normalized().transp()*b.normalized()))[0]);
  double t = (a.transp()*b)[0];
  double tt = a.length()*b.length();
  return acos(t/tt);
}

const icl::geom::GeomColor getDefaultSegmentColor(int id);

inline Vec3D TransformationToTranslation(const Mat4D& transformation){
  return Vec3D(transformation[3],transformation[7],transformation[11]);
}

inline Mat3D TransformationToRotation(const Mat4D& transformation){
  return Mat3D(transformation[0],transformation[1],transformation[2],
               transformation[4],transformation[5],transformation[6],
               transformation[8],transformation[9],transformation[10]);
}

inline Mat4D invertTransformation(const Mat4D& t){
  // inverse transformation
  //
  // T = ( R P )     T.inv = (R.inv() -R.inv()*P)
  //     ( 0 1 )             ( 0          1     )
  //
  // R.inv() = R.transp()
  //
  return Mat4D(t[0], t[4], t[8], -(t[3]*t[0]+t[7]*t[4]+t[11]*t[8]),
             t[1], t[5], t[9], -(t[3]*t[1]+t[7]*t[5]+t[11]*t[9]),
             t[2], t[6], t[10], -(t[3]*t[2]+t[7]*t[6]+t[11]*t[10]),
             0, 0, 0, 1);
}

/**
 * Magic variadic templating template.
 */
template<typename...Ts>
void ignore_returnvalues(Ts&&...) {}

} // namespace utils
