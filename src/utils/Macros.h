/********************************************************************
**                                                                 **
** File   : src/utils/Macros.h                                     **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

// this header is used to define some macros.
// please do not use it in other headers.

#include <iostream>
#include <utils/easylogging++.h>

// define own logging macros
#define LOG_TRACE LOG(TRACE)
#define LOG_DEBUG LOG(DEBUG)
#define LOG_FATAL LOG(FATAL)
#define LOG_ERROR LOG(ERROR)
#define LOG_WARNING LOG(WARNING)
#define LOG_INFO LOG(INFO)
#define LOG_VERBOSE LOG(VERBOSE)
#define LOG_UNKNOWN LOG(UNKNOWN)

#define NOT_IMPLEMENTED_EXIT LOG_FATAL << "Not implemented. exit"; exit(-1);



// throws exception x with streamed error y
#define THROW(x,y)\
{ \
  std::stringstream s;\
  s << y;\
  LOG_DEBUG << "Throwing exception with message: " << s.str();\
  throw x(s.str());\
}

// throws x(y) if first b is false
#define ASSERT_THROW(b,x,y){if(!(b)){THROW(x,y)}}

// returns if b is false
#define ASSERT_RETURN(b){if(!(b)){return;}}

// returns r if b is false
#define ASSERT_RETURN_DATA(b,r){if(!(b)){return r;}}
#define ASSERT_RETURN_FALSE(b){if(!(b)){return false;}}
#define ASSERT_RETURN_TRUE(b){if(!(b)){return true;}}
#define ASSERT_CONTINUE(b){if(!(b)){continue;}}

// tries x. catches everthing and rethrows it
#define TRY_RETHROW(x)\
{ \
  try { \
    x; \
  } catch (const std::exception& e){ \
    THROW(std::exception, "Rethrowing " << e.what()); \
  } catch (...) { \
    THROW(std::exception, "Rethrowing unknown exception."); \
  }\
}

// tries x. catches everthing and rethrows it with a message
#define TRY_RETHROW_MESSAGE(x,m)\
{ \
  try { \
    x; \
  } catch (const std::exception& e){ \
    THROW(std::exception, m << " Error: " << e.what()); \
  } catch (...) { \
    THROW(std::exception,"Rethrowing unknown exception."); \
  }\
}

// tries x prints y on exception
#define TRY_PRINT(x,y)\
{ \
  try { \
    TRY_RETHROW(x); \
  } catch (const std::exception& e){ \
    LOG_DEBUG << y << "\nGot: " << e.what(); \
  } catch (...) { \
    LOG_WARNING << y << "\n Got unknown exception. ST: " << el::base::debug::StackTrace();\
  }\
}
