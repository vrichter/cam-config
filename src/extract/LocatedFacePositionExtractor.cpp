/********************************************************************
**                                                                 **
** File   : src/extract/LocatedFacePositionExtractor.cpp           **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <rst/vision/LocatedFaces.pb.h>

#include <extract/LocatedFacePositionExtractor.h>
#include <extract/PositionExtractorRepository.h>
#include <utils/RsbHelpers.h>

#include <utils/Macros.h>

using namespace extract;

transform::Poses LocatedFacePositionExtractor::extract(DataPtr data) const {
  transform::Poses result;
  for (const rst::vision::LocatedFace& face : data->located_faces()){
    utils::rsbhelpers::optional_push_back(result,optionalPose(face));
  }
  return result;
}

namespace {
  PositionExtractorRepository::Registry::Registrar LFPEregister(
      "rst::vision::LocatedFaces",
      PositionExtractor::Ptr(new LocatedFacePositionExtractor())
      );
}
