/********************************************************************
**                                                                 **
** File   : src/extract/RstPositionExtractor.h                     **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once
#include <extract/PositionExtractor.h>
#include <utils/RsbHelpers.h>
#include <rst/geometry/Translation.pb.h>
#include <rst/geometry/Rotation.pb.h>

namespace extract {

  template<typename RstType>
  class RstPositionExtractor : public PositionExtractor {
  public:
    typedef std::shared_ptr<RstPositionExtractor<RstType>> Ptr;
    typedef boost::shared_ptr<RstType> DataPtr;

    RstPositionExtractor(){ utils::rsbhelpers::register_rst<RstType>(); }

    virtual ~RstPositionExtractor() override = default;

    virtual transform::Poses extract(InputData data) const override final {
      auto casted = boost::static_pointer_cast<RstType>(data);
      return extract(casted);
    }

    virtual transform::Poses extract(DataPtr data) const = 0;

    transform::Position locationFromTranslation(const rst::geometry::Translation& translation) const {
      return transform::Position(translation.x(),translation.y(),translation.z(),
                                 (translation.has_frame_id()) ? translation.frame_id() : "");
    }

    transform::Orientation orientationFromRotation(const rst::geometry::Rotation& rotation) const {
      return transform::Orientation(
            rotation.qw(),rotation.qx(),rotation.qy(),rotation.qz(),
            (rotation.has_frame_id()) ? rotation.frame_id() : "");
    }

    template<typename LocatedType>
    boost::optional<transform::Pose> optionalPose(const LocatedType located) const {
      boost::optional<transform::Position> position =
            boost::make_optional(located.has_location(),
                                 locationFromTranslation(located.location()));
      boost::optional<transform::Orientation> orientation =
            boost::make_optional(located.has_orientation(),
                                 orientationFromRotation(located.orientation()));
      return boost::make_optional((position || orientation),transform::Pose(position,orientation));
    }
  };

} // namespace extract
