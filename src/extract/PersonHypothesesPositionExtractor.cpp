/********************************************************************
**                                                                 **
** File   : src/extract/PersonHypothesesPositionExtractor.cpp      **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <extract/PersonHypothesesPositionExtractor.h>
#include <extract/PositionExtractorRepository.h>
#include <utils/RsbHelpers.h>

#include <utils/Macros.h>

using namespace extract;

transform::Poses PersonHypothesesPositionExtractor::extract(DataPtr data) const {
  transform::Poses result;
  for (const rst::hri::PersonHypothesis& person : data->persons()){
    if(person.has_body()){
      utils::rsbhelpers::optional_push_back(result,optionalPose(person.body()));
    }
    if(person.has_face()){
      utils::rsbhelpers::optional_push_back(result,optionalPose(person.face()));
    }
  }
  return result;
}

namespace {
  PositionExtractorRepository::Registry::Registrar LFPEregister(
      "rst::hri::PersonHypotheses",
      PositionExtractor::Ptr(new PersonHypothesesPositionExtractor())
      );
}
