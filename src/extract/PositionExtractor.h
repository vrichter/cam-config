/********************************************************************
**                                                                 **
** File   : src/extract/PositionExtractor.h                        **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <memory>
#include <transform/Pose.h>
#include <utils/Types.h>

namespace extract {

  class PositionExtractor : public boost::noncopyable {
  public:
    typedef std::shared_ptr<PositionExtractor> Ptr;
    typedef boost::shared_ptr<void> InputData;

  protected:
    PositionExtractor() = default;

  public:
    virtual ~PositionExtractor() = default;
    virtual transform::Poses extract(InputData data) const = 0;
  };

} // namespace extract
