/********************************************************************
**                                                                 **
** File   : src/extract/PersonHypothesesPositionExtractor.h        **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once
#include <extract/RstPositionExtractor.h>
#include <rst/hri/PersonHypotheses.pb.h>

namespace extract {

  class PersonHypothesesPositionExtractor : public RstPositionExtractor<rst::hri::PersonHypotheses> {
  public:

    PersonHypothesesPositionExtractor() = default;

    virtual ~PersonHypothesesPositionExtractor() override = default;

    virtual transform::Poses extract(DataPtr data) const override;
  };

} // namespace extract
