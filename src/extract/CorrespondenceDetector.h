/********************************************************************
**                                                                 **
** File   : src/extract/CorrespondenceDetector.h                   **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <memory>
#include <vector>
#include <mutex>
#include <utils/Subject.h>
#include <transform/Pose.h>

namespace extract {

  struct PosePair {
    PosePair(transform::Pose one, transform::Pose two) : first(one), second(two) {}

    transform::Pose first;
    transform::Pose second;
  };

  class CorrespondenceDetector : public utils::Subject<PosePair> {
  public:
    typedef std::shared_ptr<CorrespondenceDetector> Ptr;

  protected:
    CorrespondenceDetector() = default;

  public:
    virtual ~CorrespondenceDetector() = default;
  };

  class FeatureFilter : public utils::Subject<transform::TimedPose> {
  public:
    typedef std::shared_ptr<FeatureFilter> Ptr;

    virtual void push(transform::TimedPoseSet poses) = 0;

    virtual ~FeatureFilter() = default;

  protected:
    FeatureFilter() = default;
  };

  class OneOrNothingFeatureFilter : public FeatureFilter {
  public:
    virtual ~OneOrNothingFeatureFilter() = default;

    void push(transform::TimedPoseSet poses){
      if(poses.poses.size() == 1){
        notify(transform::TimedPose(poses.poses.front(),poses.timestamp));
      }
    }
  };

  struct TimeDeltaSimilarity {
    static double distance(const transform::TimedPose& first, const transform::TimedPose& second){
      return std::abs(first.timestamp() - second.timestamp());
    }
  };

  template<int MAX>
  struct MaxSizeRetention {
    static void filter(std::vector<transform::TimedPose>& first, std::vector<transform::TimedPose>& second){
      resize(first);
      resize(second);
    }

  private:
    static void resize(std::vector<transform::TimedPose>& vector){
      return;
      if(vector.size() > MAX){
        std::copy(vector.end() - MAX, vector.end(),vector.begin());
        vector.erase(vector.begin() + MAX, vector.end());
      }
    }
  };

  template<typename FeatureSimilarity = TimeDeltaSimilarity,
           typename RetentionStrategy = MaxSizeRetention<100>>
  class SimpleCorrespondenceDetector : public CorrespondenceDetector {
  public:
    typedef std::shared_ptr<SimpleCorrespondenceDetector> Ptr;

    void pushGlobal(transform::TimedPose pose){
      std::unique_lock<std::recursive_mutex> lock(mutex);
      global_results.push_back(pose);
      calculateCorrespondences();
    }

    void pushLocal(transform::TimedPose pose){
      std::unique_lock<std::recursive_mutex> lock(mutex);
      local_results.push_back(pose);
      calculateCorrespondences();
    }

    void calculateCorrespondences(){
      std::unique_lock<std::recursive_mutex> lock(mutex);
      for(auto local_it = local_results.begin(); local_it != local_results.end();){
        auto global_it = find_best_global_match(*local_it);
        if(global_it != global_results.end()){
          // found correspondence
          notify(PosePair(local_it->pose(),global_it->pose()));
          local_it = local_results.erase(local_it);
          global_results.erase(global_it);
        } else {
          ++local_it;
        }
      }
      RetentionStrategy::filter(local_results,global_results);
    }

    std::vector<transform::TimedPose>::iterator find_best_global_match(const transform::TimedPose& pose){
      std::unique_lock<std::recursive_mutex> lock(mutex);
      double best_dist = std::numeric_limits<double>::max();
      auto best_it = global_results.end();
      for(auto it = global_results.begin(); it != global_results.end(); ++it){
        double dist = FeatureSimilarity::distance(pose,*it);
        if(dist < best_dist){
          best_dist = dist;
          best_it = it;
        }
      }
      return best_it;
    }


    private:
      std::recursive_mutex mutex;
      std::vector<transform::TimedPose> global_results;
      std::vector<transform::TimedPose> local_results;
  };

} // namespace extract
