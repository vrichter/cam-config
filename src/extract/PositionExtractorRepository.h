/********************************************************************
**                                                                 **
** File   : src/extract/PositionExtractorRepository.h              **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <utils/ObjectRegistry.h>
#include <extract/PositionExtractor.h>

namespace extract {

class PositionExtractorRepository {
public:

  /**
   * @brief Registry static registry of extractor implementations.
   */
  typedef utils::StaticRegistry<
    std::string, // id
    PositionExtractor::Ptr, // saved type
    PositionExtractorRepository // make ist unique
  > Registry;

  PositionExtractorRepository() = default;

  PositionExtractor::Ptr getExtractor(const std::string& id){
    return Registry::registry().getObject(id).get_value_or(PositionExtractor::Ptr());
  }
};

} // namespect extract
