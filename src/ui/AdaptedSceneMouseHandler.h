/********************************************************************
**                                                                 **
** Copyright (C) 2013 Viktor Richter                               **
**                                                                 **
** File   : src/ui/AdaptedSceneMouseHandler.h                       **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <ICLQt/MouseHandler.h>
#include <ICLUtils/Mutex.h>
#include <ICLUtils/Point.h>

namespace ui {

/// A handler for mouse inputs of the icl scene.
/**
This implementation processes mouse inputs.
When the mouse is clicked while the 'shift' modifier is active the position of
the click is saved.
**/
struct AdaptedSceneMouseHandler : public icl::qt::MouseHandler {

  public:
    /// creates a handler instance.
    /**
        sets the click position to (-1,-1) and the click state to false.
        @param a pointer to the original mouse handler of the scene
    **/
    AdaptedSceneMouseHandler(icl::qt::MouseHandler*h);

    /// Processes mouse events, saving a click position when 'shift' is active.
    void process(const icl::qt::MouseEvent& e);

    /// a getter for the current clicked state
    /**
    @return whether a click event was triggered since the last call of getClickPoint()
    **/
    bool isClicked();

    /// a getter for the last click point
    /**
      returns the position of the last click, setting the click position
      to (-1,-1) and isClicked() to false.
      @return the position of the last click or (-1,-1) when no click is available
    **/
    icl::utils::Point getClickPoint();

  private:
    /// Mutex for concurrency issues
    icl::utils::Mutex m_Mutex;
    /// the original mouse handler of the scene
    icl::qt::MouseHandler* m_MouseHandler;
    /// holds the last click point
    icl::utils::Point m_Clicked;
};

} // namespace ui
