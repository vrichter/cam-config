/********************************************************************
**                                                                 **
** Copyright (C) 2013 Viktor Richter                               **
**                                                                 **
** File   : src/ui/AdaptedSceneMouseHandler.cpp                     **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <ui/AdaptedSceneMouseHandler.h>

using namespace ui;

AdaptedSceneMouseHandler::AdaptedSceneMouseHandler(MouseHandler *h)
  : m_MouseHandler(h), m_Clicked(-1,-1)
{}

void AdaptedSceneMouseHandler::process(const icl::qt::MouseEvent &e){
  icl::utils::Mutex::Locker l(m_Mutex);
  if(e.isPressEvent() && e.isModifierActive(icl::qt::ShiftModifier)){
    m_Clicked = e.getPos();
  }
  m_MouseHandler->process(e);
}

bool AdaptedSceneMouseHandler::isClicked(){
  icl::utils::Mutex::Locker l(m_Mutex);
  bool ret = !(m_Clicked.x < 0);
  return ret;
}

icl::utils::Point AdaptedSceneMouseHandler::getClickPoint(){
  icl::utils::Mutex::Locker l(m_Mutex);
  icl::utils::Point ret = m_Clicked;
  m_Clicked = icl::utils::Point(-1,-1);
  return ret;
}
