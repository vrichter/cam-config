/********************************************************************
**                                                                 **
** File   : src/transform/FacesHandler.h                           **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <transform/PositionEstimation.h>
#include <rsb/Handler.h>
#include <rsb/Informer.h>
#include <rst/vision/LocatedFaces.pb.h>

namespace transform {

class FacesHandler : public rsb::EventFunctionHandler {

public:

  FacesHandler(const PositionEstimation& estimation, rsb::Scope publish_scope,
               uint face_width_mm = default_face_width_mm());

  void handle(rsb::EventPtr event);

  /*
   * The real width of a human face is somewhere around 150mm.
   */
  static uint default_face_width_mm() { return 150; }

private:
  PositionEstimation m_Estimation;
  boost::shared_ptr<rsb::Informer<rst::vision::LocatedFaces> > m_Informer;
  const uint m_FaceWidthMM;

};

} // namespace transform
