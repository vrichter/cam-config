/********************************************************************
**                                                                 **
** File   : src/transform/PositionProvider.h                       **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once


#include <rsb/Handler.h>
#include <rsb/Listener.h>

#include <extract/PositionExtractor.h>
#include <transform/Pose.h>
#include <utils/Types.h>
#include <utils/Subject.h>

namespace transform {

class PositionProvider : public utils::Subject<TimedPoseSet> {

protected:
  PositionProvider() = default;

public:
  typedef std::shared_ptr<PositionProvider> Ptr;
  virtual ~PositionProvider() = default;
};

class RsbPositionProvider : public PositionProvider {
public:
  RsbPositionProvider(const std::string& scope);

  virtual ~RsbPositionProvider();

private:
  rsb::ListenerPtr m_Listener;
  rsb::HandlerPtr m_Handler;

  void handle(rsb::EventPtr event);
};

} // namespace transform
