/********************************************************************
**                                                                 **
** File   : src/transform/PostitionEstimation.h                    **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <ICLGeom/Camera.h>
#include <utils/Types.h>

namespace transform {

class PositionEstimation {
private:
  icl::geom::Camera m_Camera;
  icl::utils::Array2D<icl::geom::ViewRay> m_ViewRays;
  uint m_ObjectWidthMM;

public:
  PositionEstimation(const icl::geom::Camera& camera);

  float calculateDistance(const icl::geom::ViewRay& left, const icl::geom::ViewRay& right, double real_object_width_mm);

  float calculateDistance(uint x1, uint y1, uint x2, uint y2, double real_object_width_mm);

  utils::Vec3D positionFromDistance(uint x, uint y, float distance);

  const uint camWidth() const;

  const uint camHeight() const;

  const std::string& camName() const;
};

} // namespace transform
