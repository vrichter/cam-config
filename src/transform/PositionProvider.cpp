/********************************************************************
**                                                                 **
** File   : src/transform/PositionProvider.cpp                     **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <rsb/Factory.h>
#include <rsb/Scope.h>
#include <rsb/EventId.h>
#include <rsb/MetaData.h>

#include <transform/PositionProvider.h>
#include <extract/PositionExtractorRepository.h>
#include <utils/Macros.h>

using namespace transform;

RsbPositionProvider::RsbPositionProvider(const std::string& scope) {
  m_Handler = rsb::HandlerPtr(new rsb::EventFunctionHandler(
                                [this] (rsb::EventPtr event) { this -> handle(event); })
                              );

  m_Listener = rsb::getFactory().createListener(scope);
  m_Listener->addHandler(m_Handler, true);
}

RsbPositionProvider::~RsbPositionProvider(){
  m_Listener->removeHandler(m_Handler, true);
}

void RsbPositionProvider::handle(rsb::EventPtr event){
  TimedPoseSet data;
  try {
    extract::PositionExtractorRepository repo;
    auto extractor = repo.getExtractor(event->getType());
    if(extractor){
      data.poses = extractor->extract(event->getData());
    } else {
      LOG_WARNING << "Could not find an extractor for '" << event->getType() << "'";
    }
  } catch (std::exception& e){
    LOG_DEBUG << "Could not extract poses from Event with type = '" << event->getType() << "'. Error: " << e.what();
  } catch (...) {
    LOG_DEBUG << "Could not extract poses from Event with type = '" << event->getType() << "'. Unknown error";
  }
  if(!data.poses.empty()){
    data.timestamp = event->getMetaData().getCreateTime();
    notify(data);
  }
}
