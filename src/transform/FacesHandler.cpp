/********************************************************************
**                                                                 **
** File   : src/transform/FacesHandler.cpp                         **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <transform/FacesHandler.h>
#include <rsb/Factory.h>
#include <rsb/Scope.h>
#include <rst/vision/Faces.pb.h>

#include <utils/Macros.h>

using namespace transform;

FacesHandler::FacesHandler(const PositionEstimation& estimation, rsb::Scope publish_scope,
                           uint face_width_mm)
  : rsb::EventFunctionHandler(boost::bind(&FacesHandler::handle,this,_1)),
    m_Estimation(estimation), m_FaceWidthMM(face_width_mm)
{
  m_Informer = rsb::getFactory().createInformer<rst::vision::LocatedFaces>(publish_scope);
}

void FacesHandler::handle(rsb::EventPtr event){
  auto faces = boost::static_pointer_cast<rst::vision::Faces>(event->getData());

  auto message = boost::shared_ptr<rst::vision::LocatedFaces>(rst::vision::LocatedFaces::default_instance().New());
  message->set_width(faces->width());
  message->set_height(faces->height());

  double widthC = m_Estimation.camWidth() / faces->width();
  double heightC = m_Estimation.camHeight() / faces->height();
  for(int i = 0; i < faces->faces_size(); ++i){
    auto faceRegion = faces->faces(i).region();
    auto left = faceRegion.top_left();
    auto dist = m_Estimation.calculateDistance(
          left.x() * widthC,
          left.y() * heightC,
          (left.x()+faceRegion.width()) * widthC,
          left.y() * heightC,
          m_FaceWidthMM);
    std::cout << "face " << i << " distance " << dist << std::endl;

    utils::Vec3D position = m_Estimation.positionFromDistance(
          (left.x() + faceRegion.width()/2) * widthC,
          (left.y() + faceRegion.height()/2) * heightC,
          dist
          );
    auto face = message->add_located_faces();
    face->mutable_face()->operator =(faces->faces(i));
    face->mutable_location()->set_x(position[0]/1000);
    face->mutable_location()->set_y(position[1]/1000);
    face->mutable_location()->set_z(position[2]/1000);
    face->mutable_location()->set_frame_id(m_Estimation.camName());
  }
  LOG_INFO << "located faces: " << message->ShortDebugString();
  m_Informer->publish(message);
}
