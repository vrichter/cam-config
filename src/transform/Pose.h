/********************************************************************
**                                                                 **
** File   : src/transform/Pose.h                                   **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <utils/Types.h>
#include <boost/optional.hpp>

namespace transform {

class Position {
public:

  Position(double x=0, double y=0, double z=0, const std::string& frame = "")
    : m_X(x), m_Y(y), m_Z(z), m_Frame(frame) {}

  const double& x() const { return m_X; }
  const double& y() const { return m_Y; }
  const double& z() const { return m_Z; }

  const std::string& frame() const { return m_Frame; }

private:
  double m_X;
  double m_Y;
  double m_Z;
  std::string m_Frame;
};

class Orientation {
public:

  Orientation(double qw=1, double qx=0, double qy=0, double qz=0, const std::string& frame = "")
    : m_Qw(qw), m_Qx(qx), m_Qy(qy), m_Qz(qz), m_Frame(frame) {}

  const double& qw() const { return m_Qw; }
  const double& qx() const { return m_Qx; }
  const double& qy() const { return m_Qy; }
  const double& qz() const { return m_Qw; }
  const std::string& frame() const { return m_Frame; }

private:
  double m_Qw;
  double m_Qx;
  double m_Qy;
  double m_Qz;
  std::string m_Frame;
};

class Pose {
public:

  Pose(const Position& position) : m_Position(position) {}
  Pose(const Orientation& orientation) : m_Orientation(orientation) {}
  Pose(Position position, Orientation orientation)
    : m_Position(position), m_Orientation(orientation) {}
  Pose(boost::optional<Position> position, boost::optional<Orientation> orientation)
    : m_Position(position), m_Orientation(orientation) {}

  const boost::optional<Position>& position() const { return m_Position; }
  const boost::optional<Orientation>& orientation() const { return m_Orientation; }

  static void print(std::ostream& stream, const std::vector<Pose>& poses);

private:
  boost::optional<Position> m_Position;
  boost::optional<Orientation> m_Orientation;
};

typedef std::vector<Pose> Poses;

typedef uint64_t Timestamp;

class TimedPose {
public:
  TimedPose(const Pose& pose, uint64_t timestamp) : m_Timestamp(timestamp), m_Pose(pose) {}
  TimedPose(const TimedPose& other) : m_Timestamp(other.timestamp()), m_Pose(other.pose()) {}

  const Timestamp& timestamp() const { return m_Timestamp; }
  const Pose& pose() const { return m_Pose; }

private:
  Timestamp m_Timestamp;
  Pose m_Pose;
};

typedef std::vector<TimedPose> TimedPoses;

struct TimedPoseSet {
  Timestamp timestamp;
  Poses poses;
};

std::ostream & operator<<(std::ostream &out, const Position& pos);

std::ostream & operator<<(std::ostream &out, const Orientation& orient);

std::ostream & operator<<(std::ostream &out, const Pose& pose);

std::ostream & operator<<(std::ostream &os, const TimedPose& tpos);

std::ostream & operator<<(std::ostream &os, const TimedPoseSet& tpos);

} // namespace transform
