/********************************************************************
**                                                                 **
** File   : src/transform/PostitionEstimation.cpp                  **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <transform/PositionEstimation.h>
#include <utils/Types.h>
#include <utils/Macros.h>

using namespace transform;

PositionEstimation::PositionEstimation(const icl::geom::Camera& camera)
  : m_Camera(camera), m_ViewRays(m_Camera.getAllViewRays())
{}

float PositionEstimation::calculateDistance(
    const icl::geom::ViewRay& left,
    const icl::geom::ViewRay& right,
    double real_object_width_mm)
{
  /*
   * A ViewRay is a point and a direction.
   *                            left  := p1 + lambda1 * n1
   *                            right := p2 + lambda2 * n2
   *
   * Assuming:
   *                               p1 == p2
   *                          lambda1 == lambda2
   *                           | n1 | == 1
   *                           | n2 | == 1
   *
   * Using the itercept theorem.
   *    | p1 + lambda * n1 | / | n1 |  = real_object_width_mm / | n2 - n1 |
   * ->          | p1 + lambda * n1 |  = real_object_width_mm * | n1 | * 1/| n2 - n1 |
   * ->                        lambda  = real_object_width_mm * | n1 | * 1 / | n2 - n1 |
   */
  ASSERT_THROW(left.offset == right.offset,std::invalid_argument,
               "ViewRays offset must be equal.");
  double ln1l = utils::toVec3D(left.direction).length();
  double ln1_n2l = utils::toVec3D(right.direction - left.direction).length();
  ASSERT_THROW(ln1_n2l != 0.f && ln1_n2l != 2.f,std::invalid_argument,
               "ViewRays cannot point in same or opposing directions.");
  return real_object_width_mm * ln1l * (1 / ln1_n2l);
}

float PositionEstimation::calculateDistance(uint x1, uint y1, uint x2, uint y2, double real_object_width_mm)
{
  const auto& left = m_ViewRays(x1,y2);
  const auto& right = m_ViewRays(x2,y2);
  return calculateDistance(left,right,real_object_width_mm);
}

utils::Vec3D PositionEstimation::positionFromDistance(uint x, uint y, float distance){
  const auto& ray = m_ViewRays(x,y);
  return utils::toVec3D(ray.offset + ray.direction * distance);
}

const uint PositionEstimation::camWidth() const {
  return m_Camera.getRenderParams().chipSize.width;
}

const uint PositionEstimation::camHeight() const {
  return m_Camera.getRenderParams().chipSize.height;
}


const std::string& PositionEstimation::camName() const{
  return m_Camera.getName();
}
