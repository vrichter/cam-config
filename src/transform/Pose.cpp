/********************************************************************
**                                                                 **
** File   : src/transform/Pose.cpp                                 **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <transform/Pose.h>

using namespace transform;

static const std::string INDENT = "  ";

void Pose::print(std::ostream &out, const Poses &poses){
    out << "[";
    if(poses.size() > 0){
      out << INDENT;
      out << poses.at(0);
    }
    for(uint i = 1; i < poses.size(); ++i){
      out << "," << INDENT;
      out << poses.at(i);
    }
    out << INDENT << "]";
}

std::ostream & transform::operator<<(std::ostream &out, const Position& pos)
{
    out << "{"
        <<  "  x: "     << pos.x()
        << ",  y: "     << pos.y()
        << ",  z: "     << pos.z()
        << ",  frame: " << pos.frame()
        << "  }";
    return out;
}

std::ostream & transform::operator<<(std::ostream &out, const Orientation& orient)
{
    out << "{"
        <<  "  qw: "    << orient.qw()
        << ",  qx: "    << orient.qx()
        << ",  qy: "    << orient.qy()
        << ",  qz: "    << orient.qz()
        << ",  frame: " << orient.frame()
        << "  }";
    return out;
}

std::ostream & transform::operator<<(std::ostream &out, const Pose& pose)
{
    out << "{";
    if(pose.position()){
      out << "  position:  " << pose.position().get();
      if(pose.orientation()){
        out << ",";
      }
    }
    if(pose.orientation()){
      out << "  orientation:  " << pose.orientation().get();
    }
    out << "  }";
    return out;
}

std::ostream & transform::operator<<(std::ostream &os, const TimedPose& tpos)
{
  os << "timestamp:  " << tpos.timestamp() << ",  pose:  " << tpos.pose();
  return os;
}

std::ostream & transform::operator<<(std::ostream &os, const TimedPoseSet& tpos)
{
  os << "timestamp:  " << tpos.timestamp << ",  poses:  ";
  transform::Pose::print(os,tpos.poses);
  return os;
}
