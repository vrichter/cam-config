/********************************************************************
**                                                                 **
** File   : src/RSTGrabber.cpp                                     **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <rsb/Factory.h>
#include <rsb/Handler.h>
#include <rsb/converter/Repository.h>
#include <rsb/converter/ProtocolBufferConverter.h>

#include <ICLUtils/Mutex.h>
#include <ICLUtils/Thread.h>
#include <ICLCore/CCFunctions.h>

#include <io/RSTGrabber.h>
#include <rst/vision/Image.pb.h>

using namespace boost;
using namespace rsb;
using namespace rsb::converter;

using namespace icl::utils;
using namespace icl::core;

icl::core::format getIclFormat(rst::vision::Image::ColorMode mode){
  switch(mode){
    case rst::vision::Image::COLOR_GRAYSCALE:
      return icl::core::formatGray;
      break;
    case rst::vision::Image::COLOR_RGB:
    case rst::vision::Image::COLOR_BGR:
      return icl::core::formatRGB;
    case rst::vision::Image::COLOR_YUV:
    case rst::vision::Image::COLOR_YUV422:
      return icl::core::formatYUV;
    default:
      return icl::core::formatMatrix;
  }
}

icl::core::depth getIclDepth(rst::vision::Image::Depth depth){
  switch(depth){
    case rst::vision::Image::DEPTH_8U:
      return icl::core::depth8u;
    case rst::vision::Image::DEPTH_16U:
      return icl::core::depth32s;
    case rst::vision::Image::DEPTH_32F:
      return icl::core::depth32f;
    default:
      throw ICLException("Unknown rst::vision::Image::Depth. Known are 8u,16u and 32f.");
  }

}

ImgBase* createNewEmptyImage(const rst::vision::Image* reference){
  icl::utils::Size size(reference->width(),reference->height());
  icl::core::format format = getIclFormat(reference->color_mode());
  int channels = icl::core::getChannelsOfFormat(format);
  switch(reference->depth()){
    case rst::vision::Image::DEPTH_8U:
      return new icl::core::Img8u(size,channels,format);
    case rst::vision::Image::DEPTH_16U:
      return new icl::core::Img32s(size,channels,format);
    case rst::vision::Image::DEPTH_32F:
      return new icl::core::Img32f(size,channels,format);
    default:
      throw ICLException("Unknown image depth.");
  }
}

bool matches(const ImgBase* img, const rst::vision::Image* reference){
  if(img->getWidth() != (int)reference->width()) return false;
  if(img->getHeight() != (int)reference->height()) return false;
  if(img->getChannels() != (int)reference->channels()) return false;
  if(img->getFormat() != getIclFormat(reference->color_mode())) return false;
  if(img->getDepth() != getIclDepth(reference->depth())) return false;
  return true;
}

void fillImageRgb(ImgBase* dst, const rst::vision::Image* src){
  if(src->depth() != rst::vision::Image::DEPTH_8U) throw ICLException("only work with 8u.");
  if(src->channels() != 3) throw ICLException("rgb images need to have 3 channels.");
  if(src->width() * src->height() * src->channels() != (uint)dst->getDim() * dst->getChannels())
    throw ICLException("Images do not match.");

  const icl::icl8u* srcData = (const icl::icl8u*) src->data().data();
  icl::icl8u* red   = dst->as8u()->getData(0);
  icl::icl8u* green = dst->as8u()->getData(1);
  icl::icl8u* blue  = dst->as8u()->getData(2);

  if(src->data_order() == rst::vision::Image::DATA_SEPARATE){
    uint dim = dst->getDim();
    if(src->color_mode() == rst::vision::Image::COLOR_RGB){
        std::copy(srcData+dim*0,srcData+dim*1,red);
        std::copy(srcData+dim*1,srcData+dim*2,green);
        std::copy(srcData+dim*2,srcData+dim*3,blue);
    } else if (src->color_mode() == rst::vision::Image::COLOR_BGR){
        std::copy(srcData+dim*0,srcData+dim*1,blue);
        std::copy(srcData+dim*1,srcData+dim*2,green);
        std::copy(srcData+dim*2,srcData+dim*3,red);
    } else {
      throw ICLException("Unexpected rgb color mode.");
    }
  } else if (src->data_order() == rst::vision::Image::DATA_INTERLEAVED) {
    if(src->color_mode() == rst::vision::Image::COLOR_RGB){
        for(int i = 0; i < dst->getDim(); ++i){
          red[i]   = srcData[i*3+0];
          green[i] = srcData[i*3+1];
          blue[i]  = srcData[i*3+2];
        }
    } else if (src->color_mode() == rst::vision::Image::COLOR_BGR){
        for(int i = 0; i < dst->getDim(); ++i){
          red[i]   = srcData[i*3+0];
          green[i] = srcData[i*3+1];
          blue[i]  = srcData[i*3+0];
        }
    } else {
      throw ICLException("Unexpected rgb color mode.");
    }
  } else {
    throw ICLException("Unexpected data-order.");
  }
}

template<typename DEPTH_IN, typename DEPTH_OUT>
void fillImageGray(ImgBase* dst, const rst::vision::Image* src){
  if(src->color_mode() != rst::vision::Image::COLOR_GRAYSCALE) throw ICLException("only work with grayscale.");
  if(src->channels() != 1) throw ICLException("grayscale should only have 1 channel.");
  if(src->width() * src->height() * src->channels() != (uint)dst->getDim() * dst->getChannels())
    throw ICLException("Images do not match.");

  const DEPTH_IN* srcData = (const DEPTH_IN*) src->data().data();
  DEPTH_OUT* img = dst->asImg<DEPTH_OUT>()->getData(0);
  std::cout << src->width() << "x" << src->height() << " - " << src->data().size() << " : "
            << dst->getWidth() << "x" << dst->getHeight() << " - " << dst->getDim() << std::endl;
  for(int i = 0; i < dst->getDim(); ++i){
    img[i] = *srcData++;
  }
}

inline icl::core::Img8u* convertYuv422Img(const icl::icl8u* src, uint width, uint height, icl::core::Img8u* dst){
  dst -> setSize(icl::utils::Size(width, height));
  dst -> setFormat(icl::core::formatRGB);
  // draw RGB image
  icl::icl8u* rChannel = dst -> getData(0);
  icl::icl8u* gChannel = dst -> getData(1);
  icl::icl8u* bChannel = dst -> getData(2);
  icl::icl32s r,g,b;
  icl::icl8u u,y1,v,y2;
  for (int i = 0; i < dst-> getDim()/2 ; ++i){
    y1 = *src++; u = *src++; y2 = *src++; v = *src++;
    icl::core::cc_util_yuv_to_rgb(y1,u,v,r,g,b);
    *rChannel = r; ++rChannel;
    *gChannel = g; ++gChannel;
    *bChannel = b; ++bChannel;
    icl::core::cc_util_yuv_to_rgb(y2,u,v,r,g,b);
    *rChannel = r; ++rChannel;
    *gChannel = g; ++gChannel;
    *bChannel = b; ++bChannel;
  }
  return dst;
}



void fillImageYuv(ImgBase* dst, const rst::vision::Image* src){
  const icl::icl8u* srcData = (const icl::icl8u*) src->data().data();
  Img8u* img = dst->as8u();
  if(src->color_mode() == rst::vision::Image::COLOR_YUV422){
    convertYuv422Img(srcData,src->width(),src->height(),img);
  } else {
    throw ICLException("YUV conversion not implemented.");
  }
}

void convertImage(ImgBase** dst, const rst::vision::Image* src){
  if(*dst == 0){
    *dst = createNewEmptyImage(src);
  } else if(!matches(*dst,src)){
    ICL_DELETE(*dst);
    *dst = createNewEmptyImage(src);
  }
  if(src->color_mode() == rst::vision::Image::COLOR_YUV ||
     src->color_mode() == rst::vision::Image::COLOR_YUV422){
    fillImageYuv(*dst,src);
  } else if (src->color_mode() == rst::vision::Image::COLOR_RGB ||
             src->color_mode() == rst::vision::Image::COLOR_BGR){
    fillImageRgb(*dst,src);
  } else {
    fillImageGray<uint16_t,icl::icl32s>(*dst,src);
  }
}

namespace icl{
  namespace io{

    struct RSTGrabber::Data {

        Data() : mutex(Mutex::mutexTypeRecursive){}

        ListenerPtr listener;
        Mutex mutex;
        ImgBase *bufferImage;
        ImgBase *outputImage;
        bool hasNewImage;

        void update(rsb::EventPtr event, Grabber* grabber){
          Mutex::Locker lock(mutex); // "reentrant-ness" and external access
          boost::shared_ptr<rst::vision::Image> image = boost::static_pointer_cast<rst::vision::Image>(event->getData());


          convertImage(&bufferImage,image.get());
          hasNewImage = true;

          grabber->notifyNewImageAvailable(bufferImage);
        }
    };
    
    RSTGrabber::RSTGrabber(){
      m_data = 0;
    }
    
    RSTGrabber::RSTGrabber(const std::string &scope, const std::string &transportList):m_data(0){
      init(scope,transportList);
    }
    
    void RSTGrabber::init(const std::string &scope, const std::string &transportList){
      ICL_DELETE(m_data);
      m_data = new Data;
      m_data->outputImage = 0;
      m_data->bufferImage = 0;
      m_data->hasNewImage = false;
      
      Scope rsbScope(scope);

      rsb::converter::converterRepository<std::string>()->registerConverter(
            rsb::converter::Converter<std::string>::Ptr(new rsb::converter::ProtocolBufferConverter<rst::vision::Image>())
            );

      Factory &factory = rsb::getFactory();

      ParticipantConfig rsbCfg = factory.getDefaultParticipantConfig();
      typedef std::set<ParticipantConfig::Transport> TSet;
      typedef std::vector<ParticipantConfig::Transport> TVec;
      
      TSet ts2 = rsbCfg.getTransports(true);
      TVec ts(ts2.begin(),ts2.end());
      std::vector<std::string> transports = tok(transportList,",");

      for(TVec::iterator it = ts.begin(); it != ts.end(); ++it){
        ParticipantConfig::Transport &t = *it;
        if( find(transports.begin(), transports.end(), it->getName()) == transports.end() ){
          t.setEnabled(false);
        }else{
          it->setEnabled(true);
        }
      }
      rsbCfg.setTransports(TSet(ts.begin(),ts.end()));
      m_data->listener = factory.createListener(rsbScope,rsbCfg);
      m_data->listener->addHandler(
            boost::shared_ptr<rsb::EventFunctionHandler>(
              new EventFunctionHandler(boost::bind(&RSTGrabber::Data::update,m_data,_1,this))
              )
            );
      // Configurable
      addProperty("format", "info", "", "-", 0, "");
      addProperty("size", "info", "", "", 0, "");
    }
    
    RSTGrabber::~RSTGrabber(){
      if(m_data){
        ICL_DELETE(m_data->outputImage);
        ICL_DELETE(m_data->bufferImage);
      }
    }

     
    const ImgBase *RSTGrabber::acquireImage(){
      ICLASSERT_RETURN_VAL(!isNull(),0);
      Mutex::Locker lock(m_data->mutex);
      while(!m_data->bufferImage || !m_data->hasNewImage){
        m_data->mutex.unlock();
        Thread::msleep(0);
        m_data->mutex.lock();
      }
      m_data->bufferImage->deepCopy(&m_data->outputImage);
      m_data->hasNewImage = false;
      setPropertyValue("size", m_data->outputImage->getSize());
      return m_data->outputImage;
    }

    const std::vector<GrabberDeviceDescription> &RSTGrabber::getDeviceList(bool rescan){
      static std::vector<GrabberDeviceDescription> all;
      if(!rescan) return all;
      
      /// TODO: list segments

      return all;
    }

    REGISTER_CONFIGURABLE(RSTGrabber, return new RSTGrabber("/icl/foo", "spread"));

    Grabber* createRSTGrabber(const std::string &param){
      std::vector<std::string> ts = tok(param,":");
      if(!ts.size()){
        throw ICLException("invalid argument count (expected 1 or 2)");
      } else if(ts.size() == 1){
        return new RSTGrabber(ts[0]);
      } else if(ts.size() == 2){
        return new RSTGrabber(ts[1],ts[0]);
      } else {
        throw ICLException("invalid definition string (exptected: [transport-list]:scope");
      }
    }

    const std::vector<GrabberDeviceDescription>& getRSTDeviceList(std::string filter, bool rescan){
      static std::vector<GrabberDeviceDescription> deviceList;
      if(!rescan) return deviceList;

      deviceList.clear();
      // if filter exists, add grabber with filter
      if(filter.size()) deviceList.push_back(
        GrabberDeviceDescription("rst", filter, "A grabber for rst::vision::Image images published via rsb.")
        );
      return deviceList;
    }

    REGISTER_GRABBER(rst,utils::function(createRSTGrabber), utils::function(getRSTDeviceList),
                     "rst:[comma sep. transport list=spread]\\:scope:Robotics Service Bus based rst::vision::Image source");

  } // namespace io
}
