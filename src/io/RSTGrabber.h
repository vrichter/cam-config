/********************************************************************
**                                                                 **
** File   : src/RSTGrabber.h                                       **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <ICLUtils/CompatMacros.h>
#include <ICLIO/Grabber.h>

//#if !defined(HAVE_RSB) || !defined(HAVE_RST)
//  #if WIN32
//    #pragma WARNING("This header should only be included if ICL_HAVE_RSB and ICL_HAVE_RST are defined and available in ICL")
//  #else
//    #warning "This header should only be included if ICL_HAVE_RSB and ICL_HAVE_RST are defined and available in ICL"
//  #endif
//#endif

namespace icl{
  namespace io{
    
    /// Grabber implementation for RST based image transfer
    class RSTGrabber : public Grabber{
        struct Data;  //!< pimpl type
        Data *m_data; //!< pimpl pointer

      public:

        /// empty constructor (creates a null instance)
        ICLIO_API RSTGrabber();

        /// Destructor
        ICLIO_API ~RSTGrabber();

        /// main constructor with given scope and comma separated transportList
        /** supported transports are socket, spread and inprocess. Please note, that
          the spread-transport requires spread running. */
        ICLIO_API RSTGrabber(const std::string &scope, const std::string &transportList="spread");

        /// deferred intialization with given scope and comma separated transportList
        /** supported transports are socket, spread and inprocess. Please note, that
          the spread-transport requires spread running. */
        ICLIO_API void init(const std::string &scope, const std::string &transportList = "spread");

        /// grabber-interface
        ICLIO_API virtual const core::ImgBase *acquireImage();

        /// returns whether this grabber has not jet been initialized
        inline bool isNull() const { return !m_data; }

        /// returns a list of all available rsb streams
        ICLIO_API static const std::vector<GrabberDeviceDescription> &getDeviceList(bool rescan);

        /// callback for changed configurable properties
        ICLIO_API void processPropertyChange(const utils::Configurable::Property &prop);

    };

  } // namespace io
}

